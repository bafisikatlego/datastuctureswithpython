"""
     1. Python dictionaries are built-in hash tables
     2. Hash tables: Mathematical function are perfomed on the key to produce and address
     3. They key value pairs data is then stored at the address produced from hashed key
     4. Hash functions are deterministic and one way
     5. Collisions can happen when you put a key value where there is already an existign key value pair
        -- Separate chaining: putting colliding key value pairs at the same adress in a list or a linked list
        -- Linear probing: opening addressing, you probe address until you find an empty adresss to put the colliding key value pair

    6. Having a prime number of adresses for a hash table increases randomness and reduces collision

"""

class HashTable:
    def __init__(self, size=7):
        self.data_map = [None]* size
    
    def __hash(self, key):
        my_hash = 0
        for letter in key:
            my_hash = (my_hash + ord(letter) * 23) % len(self.data_map)
        return my_hash

    def print_table(self):
        for key, value in enumerate(self.data_map):
            print(key, ":", value)

    def set_item(self, key, value):
        index = self.__hash(key)
        if self.data_map[index] == None:
            self.data_map[index] = []
        self.data_map[index].append([key,value])
    
    def get_item(self, key):
        index = self.__hash(key)
        if self.data_map[index] is not None:
            for item in range(len(self.data_map[index])):
                if self.data_map[index][item][0] == key:
                    return self.data_map[index][item][1]
        return None

    def keys(self):
        all_keys = []
        for index in range(len(self.data_map)):
            if self.data_map[index] is not None:
                for entry in range(len(self.data_map[index])):
                    all_keys.append(self.data_map[index][entry][0])
        return all_keys

if __name__ == "__main__":
    h1 = HashTable()

    h1.set_item('bolts', 1400)
    h1.set_item('washers', 50)
    h1.set_item('lumber', 70)
    print("\n# Printing hash table")
    h1.print_table()

    print("\n# Get items from Hash table")
    print(h1.get_item("lumber"))
    print(h1.get_item("washers"))
    print(h1.get_item("not-existing"))

    print("\n# Get all keys from Hash table")
    print(h1.keys())
