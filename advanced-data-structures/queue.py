"""
     1. I use a queue of people to visualize the queue data structure.
     2. Uses Firt In First Out, FIFO or Last in Last Out, LILO
     3. We can only add(enqueue) elements at the end of queue, but remove(dequeue) at the begining of queue
     4. Multiple ways to implement a queue

        - Simplest is with List, but removing and adding will be O(n) at the index 0
        - Memory efficent is with Linked Lists
            - Using Singly linked List you want to enqueue at the tail and dequeue at the head for O(1)
            - Using Doubly linked list enqueue/dequeue can happen at head or tail, still O(1)

    
"""
from linkedList import Node


class Queue:
    def __init__(self, data=None):
        if data is not None:
            self.first = self.last = Node(data)
            self.length = 1
        else:
            self.first = self.last = None
            self.length = 0

    def enqueue(self, data):
        new_node = Node(data)
        if self.length == 0:
            self.first = self.last = new_node
        else:
            self.last.next = new_node
            self.last = new_node
        self.length += 1
        return True

    def dequeue(self):
        if self.length == 0:
            raise Exception("Queue is Empty!")
        elif self.length == 1:
            result = self.first
            self.first = self.last = None
        else:
            result = self.first
            self.first = result.next
            result.next = None
        self.length -= 1
        return result

    def print_queue(self):
        print(f"Size of Queue: {self.length}")
        temp = self.first
        while temp:
            print(temp.data, end="->")
            temp = temp.next
        print("\n")


if __name__ == "__main__":

    # Creating a Stack using above classes
    queue = Queue()  # Empty list
    print("# Print newly created queue")
    queue.print_queue()

    print("# Enqueue item in queue")
    for item in range(10):
        queue.enqueue(item)
    queue.print_queue()

    print("# Dequeue item from queue")
    print(queue.dequeue().data)
    queue.print_queue()
