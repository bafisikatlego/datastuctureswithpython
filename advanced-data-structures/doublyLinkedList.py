"""
     1. A doubly linked list is a linear data structure where elements are not stored in continous memory.
     2. Elements in the linked list are linked using pointers, both directions
     3. Entry point to a linked list is a pointer called HEAD which points to the first node on the list
     4. If empty the value of head is set to NULL
     5. Each node is a linked list consists of the following:

        -- Data
        -- next ( Reference to the next node )
        -- previous ( Reference to the previous node )
"""

from unittest import result


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.previous = None

class DoublyLinkedList:
    def __init__(self, data=None):
        if data is None:
            self.head = self.tail = None
            self.length = 0
        else: 
            # Init the list
            self.head = self.tail = Node(data)
            self.length = 1
    
    def append(self, data):
        new_node = Node(data)
        if self.length ==0:
            self.head = self.tail = new_node
        else:
            self.tail.next = new_node
            new_node.previous = self.tail
            self.tail = new_node
        
        self.length += 1
        return True

    def pop(self):
        if self.length == 0:
            raise Exception("List is empty!")
        elif self.length == 1:
            result = self.head
            self.head = self.tail = None
        else:
            result = self.tail
            self.tail = self.tail.previous
            self.tail.next = None
            result.previous = None
        self.length -= 1
        return result

    def prepend(self, data):
        new_node = Node(data)
        if self.length == 0:
            self.head = self.tail = new_node
        else:
            new_node.next = self.head
            self.head.previous = new_node
            self.head = new_node
        self.length += 1
        return True

    def pop_first(self):
        result = self.head
        if self.length == 0:
            raise Exception("List is empty!")
        elif self.length == 1:
            self.head = self.tail = None
        else:
            self.head = self.head.next
            self.head.previous = None
            result.next = None
        self.length -= 1
        return result

    def get(self, index):
        if index >= self.length or index < 0:
            raise Exception("Out of range")
        else:
            if index < self.length/2:
                result = self.head
                for _ in range(index):
                    result = result.next
            else:
                result = self.tail
                for _ in range(self.length -1, index, -1):
                    result = result.previous
        return result
    
    def set_data(self, index, data):
        node_to_set = self.get(index)
        node_to_set.data = data
        return True

    def insert(self, index, data):
        new_node = Node(data)
        if index == 0:
            return self.prepend(data)
        elif index == self.length:
            return self.append(data)
        else:
            previous_node_at_index = self.get(index -1)
            current_node_at_index = previous_node_at_index.next

            new_node.next = current_node_at_index
            new_node.previous = previous_node_at_index
            previous_node_at_index.next = new_node
            current_node_at_index.previous = new_node
        self.length += 1
        return True

    def remove(self, index):
        if index == 0:
            self.pop_first()
        elif index == self.length -1:
            self.pop()
        else:
            node_to_remove = self.get(index)
            before = node_to_remove.previous
            after = node_to_remove.next
            before.next = after
            after.previous = before
            node_to_remove.next = node_to_remove.previous = None
            self.length -= 1
        return True

    def reverse(self):

        if self.length == 0:
            raise Exception("List is empty")
        elif self.length == 1:
            return
        else:
            # swap head and tail
            self.head, self.tail = self._swap(self.head, self.tail)

            # Traverse the list and swap the pointers
            current_node = self.head
            for _ in range(self.length):
                current_node.next, current_node.previous = self._swap(
                    current_node.next, current_node.previous)
                current_node = current_node.next
        return True


    def _swap(self, x, y):
        temp = x
        x = y
        y = temp
        return x, y
    
    def print_list(self):
        print(f"Size of List: {self.length}")
        temp = self.head
        while temp:
            print(temp.data, end="->")
            temp = temp.next
        if self.length > 0:
            print(f"\n Head: {self.head.data} Tail: {self.tail.data}")
        else:
            print("List is empty")
        print("\n")

if __name__ == "__main__":

    # Creating a linked list using above classes
    dlist = DoublyLinkedList()  # Empty list
    print("# Print newly created list")
    dlist.print_list()

    print("# Append to list")
    for item in range(11):
        dlist.append(item)
    dlist.print_list()


    print("# Pop item from list")
    print(dlist.pop().data)
    dlist.print_list()

    print("# Prepend item to list")
    dlist.prepend(100)
    dlist.print_list()

    # dlist = DoublyLinkedList(1)
    # dlist.append(2)
    print("# pop first item to list")
    print(dlist.pop_first().data)
    dlist.print_list()

    print("# Get element by index")
    print(dlist.get(9).data)
    dlist.print_list()

    print("# Set element by index")
    print(dlist.set_data(9, 100))
    dlist.print_list()

    print("# Insert element by index")
    print(dlist.insert(10, 300))
    dlist.print_list()

    print("# Remove element by index")
    print(dlist.remove(5))
    dlist.print_list()

    print("# Reverse the list")
    print(dlist.reverse())
    dlist.print_list()


