"""
     1. Trees can have multiple children
     2. Binary trees can atmost have 2 children 
     3. In Binary search trees all children on the left are smaller than the parent at any level
     4. All children on the right are greater than the parent in all levels 
     5. There is complete, perfect and balanced trees
     6. BST Big 0 is usually regarded as O(logn)
     7. In worst case it is O(n)

    
"""

class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


class BinarySearchTree:
    def __init__(self, data=None):
            self.root = Node(data) if data else None


    def insert(self, data):
        new_node = Node(data)
        if self.root is None:
            self.root = new_node
        else:
            temp = self.root
            while (True):
                if new_node.data == temp.data:
                    return False
                elif new_node.data < temp.data:
                    if temp.left is None:
                        temp.left = new_node
                        return True
                    temp = temp.left
                elif new_node.data > temp.data:
                    if temp.right is None:
                        temp.right = new_node
                        return True
                    temp = temp.right

    
    def contains(self, data):
        if self.root is None:
            raise Exception("Tree is empty")
        else:
            temp = self.root
            while temp is not None:
                if data == temp.data:
                    return True
                elif data < temp.data:
                    temp = temp.left
                else:
                    temp = temp.right
            return False
    
    def minimum_value(self):
        current_node = self.root
        while current_node.left:
            current_node = current_node.left
        return current_node


if __name__ == "__main__":

    my_tree = BinarySearchTree()
    my_tree.insert(47)
    my_tree.insert(21)
    my_tree.insert(76)
    my_tree.insert(18)
    my_tree.insert(27)
    my_tree.insert(52)
    my_tree.insert(82)


    print(my_tree.root.data)
    print(my_tree.root.left.data)
    print(my_tree.root.right.data)

    print(my_tree.contains(27))
    print(my_tree.contains(100))
    print(my_tree.minimum_value().data)
