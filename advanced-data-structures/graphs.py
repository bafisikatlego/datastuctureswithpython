"""
     1. Graphs are made up of vertices(nodes) and edges(connections)
     2. Edges between vertices can be weighted and also directional
     3. Trees are a form of graphs with limitations on vertices and edges
     4. 2 ways we typically represent graphs:
        - Adjacency Matrix
        - Adjanceny Lists
"""
class Graph:
    def __init__(self):
        self.adj_list = {}

    def add_vertex(self, vertex):
        if vertex not in self.adj_list.keys():
            self.adj_list[vertex] = []
            return True
        return False

    def add_edge(self, v1, v2):
        if all([v1 in self.adj_list.keys(), v2 in self.adj_list.keys()]):
            self.adj_list[v1].append(v2)
            self.adj_list[v2].append(v1)
            return True
        return False
    
    def remove_edge(self, v1, v2):
        if all([v1 in self.adj_list.keys(), v2 in self.adj_list.keys()]):
            try:
                self.adj_list[v1].remove(v2)
                self.adj_list[v2].remove(v1)
            except ValueError:
                pass
            return True
        return False
    
    def remove_vertex(self, vertex):
        # Case when edges are b-directional
        if vertex in self.adj_list.keys():
            for other_vertex in self.adj_list[vertex]:
                self.adj_list[other_vertex].remove(vertex)
            del self.adj_list[vertex]
            return True
        return False


    def print_graph(self):
        for vertex in self.adj_list:
            print(vertex, ":", self.adj_list[vertex])


if __name__ == "__main__":
    g1 = Graph()

    print("\n# Add vertex to a graph")
    g1.add_vertex("A")
    g1.add_vertex(1)
    g1.add_vertex(2)
    g1.add_vertex("B")
    g1.add_vertex("C")
    g1.print_graph()

    print("\n# Add edge between vertices in a graph")
    g1.add_edge(1,2)
    g1.add_edge(1,"A")
    g1.add_edge(1, "B")
    g1.add_edge(1, "C")
    g1.print_graph()

    print("\n# Remove edge between vertices in a graph")
    g1.remove_edge(1, "A")
    g1.remove_edge(2, "A")
    g1.print_graph()

    print("\n# Remove vertex from graph")
    g1.remove_vertex("C")
    g1.print_graph()
