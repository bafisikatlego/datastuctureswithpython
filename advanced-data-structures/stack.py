"""
     1. I use a stack of books to visualize the stack data structure.
     2. Uses Firt In Last Out, FILO or Last in First Out, LIFO
     3. We can only Remove/ Add elements at the top of stack, so last in
     4. Multiple ways to implement a stack

        - Simplest is with List, make sure to pop and push at the end of list so it is O(1)
        - Memory efficent is with Linked Lists
            - Singly linked List you want to push/pop at the head for O(1)
            - Doubly linked list push/pop can happen at head or tail, still O(1)

    
"""
from linkedList import Node

class Stack:
    def __init__(self, data=None):
        if data is not None:
            self.top = Node(data)
            self.height = 1
        else:
            self.top = None
            self.height = 0

    def push(self, data):
        new_node = Node(data)
        if self.height == 0:
            self.top = new_node
        else:
            new_node.next = self.top
            self.top = new_node
        self.height += 1
        return True

    def pop(self):
        if self.height == 0:
            raise Exception("Stack is Empty!")
        elif self.height == 1:
            result = self.top
            self.top = None
        else:
            result = self.top
            self.top = result.next
            result.next = None
        self.height -= 1
        return result
        
            

    def print_stack(self):
        print(f"Size of Stack: {self.height}")
        temp = self.top
        while temp:
            print(temp.data, end="->")
            temp = temp.next
        print("\n")

if __name__ == "__main__": 

    # Creating a Stack using above classes
    stack = Stack()  # Empty list
    print("# Print newly created stack")
    stack.print_stack()

    print("# Push item in stack")
    for item in range(10):
        stack.push(item)
    stack.print_stack()

    print("# Pop item from stack")
    print(stack.pop().data)
    stack.print_stack()
    
