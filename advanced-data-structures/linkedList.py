"""
     1. A linked list is a linear data structure where elements are not stored in continous memory.
     2. Elements in the linked list are linked using pointers, no indexes compared to lists
     3. Entry point to a linked list is a pointer called HEAD which points to the first node on the list
     4. If empty the value of head is set to NULL
     5. Each node is a linked list consists of the following:

        -- Data
        -- Pointer ( Reference to the next node )
"""

class Node:
    # Initialize the node object
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    # Initialize the List object
    def __init__(self, data = None):
        if data is not None:
            self.head = self.tail = Node(data)
            self.length = 1
        # Special case where the link list is created without data
        else: 
            self.head = self.tail = None
            self.length = 0

    # Print the linked list
    def print_list(self):
        print(f"Size of List: {self.length}")
        temp = self.head
        while temp:
            print(temp.data, end="->")
            temp = temp.next
        print(f"\n Head: {self.head.data} Tail: {self.tail.data}")
        print("\n")
        

    # Append to linked list
    def append(self, data):
        new_node = Node(data)
        self.length += 1 # Increase the size of the node
        # Special case where we try to append to an empty link list 
        if self.is_empty():
            self.head = self.tail = new_node
        else:
            self.tail.next = new_node
            self.tail = new_node
        return True

    # Pop item from end of the linked list
    def pop(self):

        # Edge cases
        # If empty
        if self.is_empty():
            raise Exception("Linked List is Empty")
        #  If length is 1
        elif self.length == 1:
            result = self.head
            self.head = self.tail = None
        else:
            # For everything else
            result = self.tail
            temp = previous_node = self.head
            while temp.next:
                #  The order is important here so previous_node trails temp
                previous_node = temp
                temp = temp.next
            
            self.tail = previous_node
            self.tail.next = None

        self.length -= 1
        return result

    #  Add item to begining of list
    def prepend(self, data):
        new_node = Node(data)
        if self.is_empty():
            self.head = self.tail = new_node
        else:
            new_node.next = self.head
            self.head = new_node

        self.length += 1
        return True

    def pop_first(self):
        if self.is_empty():
            raise Exception("Linked List is Empty")
        elif self.length == 1:
            result = self.head
            self.head = self.tail = None
        else:
            result = self.head
            next_node = self.head.next
            self.head = next_node

        self.length -= 1
        return result

    def get(self, index):
        if self.is_empty():
            raise Exception("Linked List is Empty")
        if index >= self.length or index < 0:
            raise Exception("Index out if range of List")
        else:
            temp = self.head
            for _ in range(index):
                temp = temp.next
            return temp

    def set_data(self, index, data):
        temp = self.get(index)
        temp.data = data
        return True
            
    def insert(self, index, data):
        if index == 0:
            return self.prepend(data)
        previous_node_at_index = self.get(index-1)
        new_node = Node(data)
        new_node.next = previous_node_at_index.next
        previous_node_at_index.next = new_node

        self.length += 1
        return True

    def remove(self, index):
        if index == 0:
            self.pop_first()
        elif index == self.length - 1:
            self.pop()
        else:
            previous_node_at_index = self.get(index - 1)
            # Using this is O(1) instead of self.get(index) which is O(n)
            current_node_at_index = previous_node_at_index.next
            previous_node_at_index.next = current_node_at_index.next
            current_node_at_index = None
            self.length -= 1
        
        return True

    def reverse(self):
        # Swicth tail & head
        temp = self.head
        self.head = self.tail
        self.tail = temp

        # We need before and after variables to reverse
        before = None
        after = temp.next

        # using the for loop
        for _ in range(self.length):
            # This order of this is very important
            after = temp.next
            temp.next = before
            before = temp
            temp = after

        return True

    def is_empty(self):
        return self.head == None

if __name__ == "__main__":

    # Creating a linked list using above classes
    llist = LinkedList() # Empty list


    print("# Append to list")
    for item in range(11):
        llist.append(item)
    llist.print_list()


    print("# Pop item from list")
    print(llist.pop().data)
    llist.print_list()

    print("# Prepend item to list")
    llist.prepend(100)
    llist.print_list()

    print("# pop first item to list")
    print(llist.pop_first().data)
    llist.print_list()

    print("# Get element by index")
    print(llist.get(9).data)
    llist.print_list()

    print("# Set element by index")
    print(llist.set_data(9, 100))
    llist.print_list()

    print("# Insert element by index")
    print(llist.insert(5, 300))
    llist.print_list()

    print("# Remove element by index")
    print(llist.remove(3))
    llist.print_list()

    print("# Reverse the list")
    print(llist.reverse())
    llist.print_list()