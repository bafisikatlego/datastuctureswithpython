'''
- Python Set is an ordered collection of data that is mutable
- A set does not allow any duplicate element.
- Sets are basically used to include membership testing and eliminating duplicate entries.
'''

set1 = set([1, 2, 'Example', 4, 'text', True])
print("Example Set: {}".format(set1))

set2 = set([1, 2, 'Example', 4, 'text', True, 4])
print("List with duplicates: {} \n Duplictates removed: {}".format(
    [1, 2, 'Example', 4, 'text', True, 4], set2))
