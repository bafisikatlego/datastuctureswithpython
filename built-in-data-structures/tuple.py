'''
- Python Tuple is a collection of data much like a list 
- Except Tuples are immutable meaning the elements in the tuple cannot be added or removed once created. 
- Just like a List, a Tuple can also contain elements of various types.
- Note: Tuples can also be created with a single element, but one element in the parentheses is not sufficient
- There must be a trailing ‘comma’ to make it a tuple.
'''

print("Sample Tuple: {}".format(
    ('Test', 'Examples')
))

# Creating tuple from a list
list = [1, 2, 3, 4, 5]
tuple1 = tuple(list)
print("Creating a tuple from a list: {}".format(tuple1))