"""
 Python Bytearray gives a mutable sequence of integers in the range 0 <= x < 256.
"""
b_array = bytearray((12, 8, 25, 2))
print("Example Bytearray: {}".format(b_array))

