'''
- Python dictionary is equivalent to hash tables in any other language with the time complexity of O(1). 
- It is an unordered collection of data values.
- Dictionary holds the key:value pair.
- The keys are of any hashable type i.e. an object whose can never change like strings, numbers, tuples, etc. '
- We can create a dictionary by using curly braces ({}) or dictionary comprehension.
'''
dict = {
    'name': "katlego",
    1: [1, 2, 3, 4]
}
print("Example Dict: {}".format(dict))

print("Accessing an emlement using key: {}".format(dict['name']))

print("Accessing an element using get method: {}".format(dict.get(1)))


print("creating a list using dictionary comprehension: {}".format(
    {x: x**2 for x in [1, 2, 3, 4, 5]}
))
