# Python Lists are just like the arrays
# An ordered collection of data.
# It is very flexible as the items in a list do not need to be of the same type.

list = [1, 2, 3, "example", True, 4.3]
print("Sample List: {}".format(list))

# Multi-dimensional list
# Nested Lists
multi_dimensional_list = [['example', 'of'], ['nested', 'lists']]
print("Multi-dimensional List: {}".format(multi_dimensional_list))

