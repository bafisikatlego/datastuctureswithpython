"""
    1. Can think of Recursion of a function that calls itself until it doesn't
    2. Pseudo code
        def open_gift_box()
            if ball:
                return ball
            open_gift_box()

        - The process of openning each new box must be the same
        - Each time we open the box we make the problem smaller

    3. Base case:  A condition which will at some point stop the function from calling it self. 
    4. Stack Overflow: If you do not have a base case an a recursive function calls itself infinitely
    5. Call stack: Must know how functions are pushed and poped from the call stack
"""
def factorial(n):
    # base case
    if n == 1:
        return 1
    # Doing something over and over with a smaller problem
    return n * factorial(n-1)



if __name__ == "__main__":

    print(factorial(4))

